const BUTTON_STYLES = {
    default: {
        'background-color': '#d0d0c9',
        'min-width': '110px',
        'border-color': 'green',
        'padding': '10px 20px',
        'font-size': '16px',
        'border-width': '1px',
        'display': 'inline-block',
        'color': 'black',
        'opacity':'0.8',
        'border':'none',
        'transition-duration': '0.4s',
        'margin': '4px 2px',
        'border-radius':'4px',
        
        
    },
    primary: {
        'background-color': '#00BCD4',
        'min-width': '110px',
        'border-color': 'green',
        'padding': '10px 20px',
        'font-size': '16px',
        'border-width': '1px',
        'display': 'inline-block',
        'color': 'black',
        'opacity':'0.8',
        'border':'none',
        'transition-duration': '0.4s',
        'margin': '4px 2px',
        'border-radius':'4px'
    },
    success: {
        'background-color': '#4CAF50',
        'min-width': '110px',
        'border-color': 'green',
        'padding': '10px 20px',
        'font-size': '16px',
        'border-width': '1px',
        'display': 'inline-block',
        'color': 'black',
        'opacity':'0.8',
        'border':'none',
        'transition-duration': '0.4s',
        'margin': '4px 2px',
        'border-radius':'4px'
    },
    danger: {
        'background-color': 'red',
        'min-width': '110px',
        'border-color': 'green',
        'padding': '10px 20px',
        'font-size': '16px',
        'border-width': '1px',
        'display': 'inline-block',
        'color': 'black',
        'opacity':'0.6',
        'border':'none',
        'transition-duration': '0.4s',
        'margin': '4px 2px',
        'border-radius':'4px'
    },
    light: {
        'background-color': 'none',
        'background':'none',
        'min-width': '110px',
        'border-color': 'green',
        'padding': '10px 20px',
        'font-size': '16px',
        'border-width': '1px',
        'display': 'inline-block',
        'color': 'black',
        'opacity':'0.8',
        'border':'none',
        'transition-duration': '0.4s',
        'margin': '4px 2px',
        'border-radius':'4px'
    },
    flat: {
        'background-color': 'none',
        'background':'none',
        'min-width': '110px',
        'padding': '10px 20px',
        'font-size': '16px',
        'display': 'inline-block',
        'color': 'black',
        'opacity':'0.8',
        'border':'1px solid currentColor',
        'transition-duration': '0.4s',
        'margin': '4px 2px',
        'border-radius':'4px'
        

    },

}

export {
    BUTTON_STYLES
}
