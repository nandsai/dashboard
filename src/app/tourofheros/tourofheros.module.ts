import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { DashboardComponent } from './Dashboard/dashboards/dashboard.component';



@NgModule({
  declarations: [HeroesComponent, HeroDetailComponent, MessagesComponent, DashboardComponent],
  imports: [
    CommonModule
  ],
exports: [HeroesComponent,HeroDetailComponent]
})
export class TourofherosModule { }
