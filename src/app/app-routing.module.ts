import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CareersComponent } from './dashboard/Pagesources/careers/careers.component';
import { Contact1Component } from './dashboard/Pagesources/contact1/contact1.component';
import { Contact2Component } from './dashboard/Pagesources/contact2/contact2.component';
import { DashbordComponent } from './dashboard/Pagesources/dashbord/dashbord.component';
import { DataComponent } from './dashboard/Pagesources/data/data.component';
import { HomeComponent } from './dashboard/Pagesources/home/home.component';
import { ProfileComponent } from './dashboard/Pagesources/profile/profile.component';
import { SidenavComponent } from './dashboard/Pagesources/sidenav/sidenav.component';
import { LoginComponent } from './login/login/login.component';
import { DashboardComponent } from './tourofheros/Dashboard/dashboards/dashboard.component';
import { HeroDetailComponent } from './tourofheros/hero-detail/hero-detail.component';
import { HeroesComponent } from './tourofheros/heroes/heroes.component';

const routes: Routes = [
  // { path: 'heroes', component: HeroesComponent },
  // { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: '/contact1', pathMatch: 'full' },
  // { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'home', component: DashboardComponent },
  { path: 'signin', component: LoginComponent },
  { path: 'contact1', component: Contact1Component },
  { path: 'contact2', component: Contact2Component },
  { path: 'careers', component: CareersComponent },
  { path: 'data', component: DataComponent },
  { path: 'dashbord', component: DashbordComponent },
  { path: 'profile', component: ProfileComponent },
  
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
