import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BUTTON_STYLES } from "../../styles.config"

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  buttonStyles: any = BUTTON_STYLES.default;
  buttonStyles1: any = BUTTON_STYLES.primary;
  buttonStyles2: any = BUTTON_STYLES.success;
  buttonStyles3: any = BUTTON_STYLES.danger;
  buttonStyles4: any = BUTTON_STYLES.light;
  buttonStyles5: any = BUTTON_STYLES.flat;

  @Input()
  set backgroundColor(value: String) {
    this.buttonStyles['background-color'] = value
  }
  @Input()
  textColor?: any;
  @Input()
  set rounded(value: Boolean) {
    if(value) this.buttonStyles['border-radius'] = '21px'
  }
  @Output() newItemEvent = new EventEmitter<object>();
  constructor() {
    // this.buttonStyles['background-color'] = `${this.backgroundColor ? this.backgroundColor: "white"} !important`;
    // this.buttonStyles['color'] = this.textColor ? this.textColor: "black";
    // if(this.rounded)
    //   this.buttonStyles['borderRadius'] = '21px';
  }

  ngOnInit(): void {

  }
  addNewItem() {
    this.newItemEvent.emit(this.buttonStyles);
  }

}
