import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './components/button/button.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import { LoginComponent } from './login/login/login.component';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeroesComponent } from './tourofheros/heroes/heroes.component';
import { HeroDetailComponent } from './tourofheros/hero-detail/hero-detail.component';
import { MessagesComponent } from './tourofheros/messages/messages.component';
import { DashboardComponent } from './tourofheros/Dashboard/dashboards/dashboard.component';
import { SidenavComponent } from './dashboard/Pagesources/sidenav/sidenav.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NavbarComponent } from './dashboard/Pagesources/navbar/navbar.component';
import { HomeComponent } from './dashboard/Pagesources/home/home.component';
import {MatListModule} from '@angular/material/list';
import { Contact1Component } from './dashboard/Pagesources/contact1/contact1.component';
import { Contact2Component } from './dashboard/Pagesources/contact2/contact2.component';
import { CareersComponent } from './dashboard/Pagesources/careers/careers.component';
import { DataComponent } from './dashboard/Pagesources/data/data.component';
import { DashbordComponent } from './dashboard/Pagesources/dashbord/dashbord.component';
import { ProfileComponent } from './dashboard/Pagesources/profile/profile.component';
@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    LoginComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    DashboardComponent,
    SidenavComponent,
    NavbarComponent,
    HomeComponent,
    Contact1Component,
    Contact2Component,
    CareersComponent,
    DataComponent,
    DashbordComponent,
    ProfileComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule
    
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
