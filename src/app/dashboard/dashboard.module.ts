import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './Pagesources/sidenav/sidenav.component';
import { NavbarComponent } from './Pagesources/navbar/navbar.component';
import { HomeComponent } from './Pagesources/home/home.component';
import { Contac1Component } from './Pagesources/contac1/contac1.component';
import { Contact2Component } from './Pagesources/contact2/contact2.component';
import { Contact1Component } from './Pagesources/contact1/contact1.component';
import { CareersComponent } from './Pagesources/careers/careers.component';
import { DataComponent } from './Pagesources/data/data.component';
import { DashboardComponent } from './Pagesources/dashboard/dashboard.component';
import { ProfileComponent } from './Pagesources/profile/profile.component';
import { DashbordComponent } from './Pagesources/dashbord/dashbord.component';



@NgModule({
  declarations: [SidenavComponent, NavbarComponent, HomeComponent, Contac1Component, Contact2Component, Contact1Component, CareersComponent, DataComponent, DashboardComponent, ProfileComponent, DashbordComponent],
  imports: [
    CommonModule
  ],
  exports :  [SidenavComponent, NavbarComponent],
})
export class DashboardModule { }
