import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @Input()
  opened: boolean = true;

  // list : any =[{ "router":"home", "name":"signup"},
  // { "router":"signin", "name":"signin"}];


  selectedSideNav: any;//contact1
  selectedTab: any;
  tabsAndSideNavs: any = [
    {
      tabName: "contact",
      tabList: [
        {
          name: "contac1",
          router: "contact1"
        },
        {
          name: "contac2",
          router: "contact2"
        }
      ]
    }, {
      tabName: "abotus",
      tabList: [
        {
          name: "careers",
          router: "careers"
        },
        {
          name: "data",
          router: "data"
        }
      ]
    }, {
      tabName: "home",
      tabList: [
        {
          name: "dashboard",
          router: "dashbord"
        },
        {
          name: "profile",
          router: "profile"
        }
      ]
    }
  ];
  // selectedTab  = this.tabsAndSideNavs.sideNavList//contact


  constructor() {

    this.selectedSideNav = this.tabsAndSideNavs[0].tabList;
    console.log(JSON.stringify(this.tabsAndSideNavs[0].tabList))

  }

  ngOnInit(): void {

  }
  linkToggler($event: any) {
    this.opened = $event;

  }
  sidebarelements(tab: string) {
    //console.log('tab', tab)
    this.selectedTab = tab;
    let tempSelectedSideNav = this.tabsAndSideNavs.find((tabAndSideNav:any) => tabAndSideNav.tabName === tab)
    if(!tempSelectedSideNav) {
      alert(`Selected tab ${tab} is not valid`)
      return
    }

    this.selectedSideNav = tempSelectedSideNav.tabList
   // alert(JSON.stringify(this.selectedSideNav))
    

  }

}