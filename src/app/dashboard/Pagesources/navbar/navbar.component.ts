import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output()
  togglesidbar = new EventEmitter<any>();
  @Output()
  sidebarelements = new EventEmitter<any>();

  isopen: boolean = true;
  bottonname: any = "home";
  navlinks = [{ "opened": true, "status": "home" }]

  constructor() { }

  ngOnInit(): void {
  }
  Toggel() {
    this.isopen = !this.isopen;

  }
  Togglecheck() {
    console.log('on click child');
    this.isopen = !this.isopen;
    this.togglesidbar.emit(this.isopen);

  }
  
  Bottonselected(data: any) {
    this.bottonname = data;
    this.sidebarelements.emit(this.bottonname);

  }

}
